<br>
@if(isset($dataTypeContent->{$row->field}))
    <?php $images = json_decode($dataTypeContent->{$row->field}); ?>
    @if($images != null)
        <div class="multiple-images" id="sortable">
            @foreach($images as $k=>$image)
                <div class="img_settings_container js-parent" data-field-name="{{ $row->field }}">
                    <img src="{{ Voyager::image( $image->original_name ) }}" data-image="{{ $image->original_name }}" data-id="{{ $dataTypeContent->getKey() }}" class="js-open-image">
                    <a href="#" class="icons icon-params voyager-params show-inputs {{ $row->field }}-{{ $k }} js-open-modal"></a>
                    <a href="#" class="icons icon-delete remove-multi-image-ext {{ $row->field }}-{{ $k }}">
                        <i class="iui-close"></i>
                    </a>

                    <div class="modal modal-params fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Obrázok {{ $image->original_name }}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <img src="{{ Voyager::image( $image->original_name ) }}" >
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="img-title">Nastavenia obrázku</h4>
                                            <div class="form-group">
                                                <label>
                                                    <p>názov:</p>
                                                    <input class="form-control" type="text" name="{{ $row->field }}_ext[{{ $loop->index }}][title]" value="{{ $image->title }}">
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    <p>alt:</p>
                                                    <input class="form-control" type="text" name="{{ $row->field }}_ext[{{ $loop->index }}][alt]" value="{{ $image->alt }}">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade modal-image text-center">
                        <div class="modal-dialog">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                            </div>
                            <div class="modal-content">
                                <div class="modal-body">
                                    <img src="{{ Voyager::image( $image->original_name ) }}" >
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
    @endif

@endif
<div class="clearfix"></div>
<div class="input-images-1"></div>
{{--<input @if($row->required == 1) required @endif type="file" name="{{ $row->field }}[]" multiple="multiple" accept="image/*" class="js-files">--}}

<script>

    document.addEventListener('DOMContentLoaded', function(){
        @if(isset($dataTypeContent->{$row->field}))
        @foreach($images as $k=>$image)
        $('.remove-multi-image-ext.{{ $row->field }}-{{ $k }}').on('click', function (e) {
            e.preventDefault();
            $file = $(this).siblings('img');

            params = {
                slug:         '{{ $dataType->slug }}',
                filename:     $file.data('image'),
                id:           $file.data('id'),
                field:        $file.parent().data('field-name'),
                multi:				true,
                _token:       '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text($file.data('image'));
            $('#confirm_delete_modal').modal('show');
        });

        @endforeach

        $('.js-open-modal').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.js-parent').find('.modal-params').modal('show');
        });

        $('.js-open-image').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.js-parent').find('.modal-image').modal('show');
        });

        @endif

    });
</script>

<style>
    /*! Image Uploader - v1.2.3 - 26/11/2019
 * Copyright (c) 2019 Christian Bayer; Licensed MIT */
    @font-face{font-family:'Image Uploader Icons';src:url(/admin-assets/fonts/iu.eot);src:url(/admin-assets/fonts/iu.eot) format('embedded-opentype'),url(/admin-assets/fonts/iu.ttf) format('truetype'),url(/admin-assets/fonts/iu.woff) format('woff'),url(/admin-assets/fonts/iu.svg) format('svg');font-weight:400;font-style:normal}[class*=iui-],[class^=iui-]{font-family:'Image Uploader Icons'!important;speak:none;font-style:normal;font-weight:400;font-variant:normal;text-transform:none;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.iui-close:before{content:"\e900"}.iui-cloud-upload:before{content:"\e901"}.image-uploader{min-height:10rem;border:1px solid #d9d9d9;position:relative}.image-uploader.drag-over{background-color:#f3f3f3}.image-uploader input[type=file]{width:0;height:0;position:absolute;z-index:-1;opacity:0}.image-uploader .upload-text{position:absolute;top:0;right:0;left:0;bottom:0;display:flex;justify-content:center;align-items:center;flex-direction:column}.image-uploader .upload-text i{display:block;font-size:3rem;margin-bottom:.5rem}.image-uploader .upload-text span{display:block}.image-uploader.has-files .upload-text{display:none}.image-uploader .uploaded{padding:.5rem;line-height:0}.image-uploader .uploaded .uploaded-image{display:inline-block;width:calc(16.6666667% - 1rem);padding-bottom:calc(16.6666667% - 1rem);height:0;position:relative;margin:.5rem;background:#f3f3f3;cursor:default}.image-uploader .uploaded .uploaded-image img{width:100%;height:100%;object-fit:cover;position:absolute}.image-uploader .uploaded .uploaded-image .delete-image{display:none;cursor:pointer;position:absolute;top:.2rem;right:.2rem;border-radius:50%;padding:.3rem;line-height:1;background-color:rgba(0,0,0,.5);-webkit-appearance:none;border:none}.image-uploader .uploaded .uploaded-image:hover .delete-image{display:block}.image-uploader .uploaded .uploaded-image .delete-image i{display:block;color:#fff;width:1.4rem;height:1.4rem;font-size:1.4rem;line-height:1.4rem}@media screen and (max-width:1366px){.image-uploader .uploaded .uploaded-image{width:calc(20% - 1rem);padding-bottom:calc(20% - 1rem)}}@media screen and (max-width:992px){.image-uploader .uploaded{padding:.4rem}.image-uploader .uploaded .uploaded-image{width:calc(25% - .8rem);padding-bottom:calc(25% - .4rem);margin:.4rem}}@media screen and (max-width:786px){.image-uploader .uploaded{padding:.3rem}.image-uploader .uploaded .uploaded-image{width:calc(33.3333333333% - .6rem);padding-bottom:calc(33.3333333333% - .3rem);margin:.3rem}}@media screen and (max-width:450px){.image-uploader .uploaded{padding:.2rem}.image-uploader .uploaded .uploaded-image{width:calc(50% - .4rem);padding-bottom:calc(50% - .4rem);margin:.2rem}}

    .multiple-images{
        display: flex;
        flex-wrap: wrap;
        margin-bottom: 10px;
        line-height: 0;
        padding: 10px;
        background-color: #f7f6f6;
        border-radius: 2px;

    }
    .multiple-images>div{
        display: flex;
        flex-direction: column;
        position: relative;
    }

    .multiple-images label{
        display: block;
    }
    .multiple-images label p{
        line-height: 6px;
    }

    [class*=iui-], [class^=iui-] {
        font-family: 'Image Uploader Icons'!important;
        speak: none;
        font-style: normal;
        font-weight: 400;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    .icons{
        display: none;
        cursor: pointer;
        position: absolute;
        right: .4rem;
        border-radius: 50%;
        padding: .3rem;
        line-height: 1;
        background-color: rgba(0,0,0,.5);
        -webkit-appearance: none;
        border: none;
    }

    .icons.icon-delete{
        top: .4rem;
    }

    .icons.icon-params{
        top: 3rem;
    }

    .icons.icon-params:before{
        display: block;
        color: #fff;
        width: 1.4rem;
        height: 1.4rem;
        font-size: 1rem;
        line-height: 1.4rem;
        text-align: center;
    }

    .img_settings_container{
        width: calc(100%/8 - 1rem);
        padding-bottom: calc(100%/8 - 1rem);
        display: inline-block;
        height: 0;
        position: relative;
        margin: .5rem;
        background: #f3f3f3;
        cursor: default;
        margin-bottom:5px;
    }

    .img_settings_container>img{
        width: 100%;
        height: 100%;
        object-fit: cover;
        position: absolute;
        border-radius: 2px;
        border:1px solid #ddd;
        cursor: pointer;
    }

    .img_settings_container:hover .icons{
        display: block;
    }

    .icon-delete i{
        display: block;
        color: #fff;
        width: 1.4rem;
        height: 1.4rem;
        font-size: 1.4rem;
        line-height: 1.4rem;
    }

    .icon-delete .iui-close:before {
        content: "\e900";
    }

    .modal img{
        max-width: 100%;
        border-radius: 2px;
    }

    .modal .img-title{
        margin-bottom: 30px;
    }

    .modal .close{
        position: absolute;
        right: 5px;
        top: 30px;
        z-index: 9;
    }

    @media ( max-width: 1200px) {
        .img_settings_container{
            width: calc(100%/4 - 1rem);
            padding-bottom: calc(100%/4 - 1rem);
        }
    }

    @media (min-width: 768px){

        .modal-dialog {
            width: 70%;
        }
        .modal-image .modal-dialog {
            max-width: 80%;
            width: fit-content;
        }
    }

</style>
