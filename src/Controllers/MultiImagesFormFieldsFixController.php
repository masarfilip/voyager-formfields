<?php

namespace App\Fijeo\FormFields\Controllers;

use App\Fijeo\FormFields\ContentTypes\KeyValueJsonContentType;
use App\Fijeo\FormFields\ContentTypes\MultiImagesContentType;
use Illuminate\Http\Request;
use CMS\Voyager\Http\Controllers\VoyagerBaseController;
use CMS\Voyager\Http\Controllers\Controller;
use CMS\Voyager\Facades\Voyager;

class MultiImagesFormFieldsFixController extends VoyagerBaseController
{

    public function getContentBasedOnType(Request $request, $slug, $row, $options = null)
    {
        switch ($row->type) {
            case 'multi_images':
                return (new MultiImagesContentType($request, $slug, $row, $options))->handle();
            case 'key-value_to_json':
                return (new KeyValueJsonContentType($request, $slug, $row, $options))->handle();

            default:
                return Controller::getContentBasedOnType($request, $slug, $row, $options);
        }
    }


    public function insertUpdateData($request, $slug, $rows, $data)
    {
        $fieldNameArr = [];
        $exFilesArr = [];

        foreach ($rows as $row) {
            if ($row->type == 'multi_images') {
                $is_multiple_image_attrs = 1;
                $fieldNameArr[] = $row->field;
                $exFilesArr[$row->field] = json_decode($data->{$row->field}, true);
                $request->except("{$row->field}");
            }
        }

        $new_data = VoyagerBaseController::insertUpdateData($request, $slug, $rows, $data);

        if (isset($is_multiple_image_attrs)) {
          foreach ($fieldNameArr as $field) {
            $end_content = [];
            foreach ($rows as $row) {
              $content = $new_data->{$field};
              if ($row->type == 'multi_images' && !is_null($content) && $exFilesArr[$field] != json_decode($content, 1)) {
                if (isset($data->{$row->field})) {
                  if (!is_null($exFilesArr[$field])) {
                    $content = json_encode(array_merge($exFilesArr[$field], json_decode($content, 1)));
                  }
                }
                $new_content = $content;
              }
            }

            if (isset($new_content)) {
              $content = json_decode($new_content, 1);
            } else {
              $content = json_decode($content, 1);
            }

            if (isset($content)) {
              foreach ($content as $i => $value) {
                if (isset($request->{$field.'_ext'}[$i])) {
                  $end_content[] = array_merge($content[$i], $request->{$field.'_ext'}[$i]);
                } else {
                  $end_content[] = $content[$i];
                }
              }

              $data->{$field} = json_encode($end_content);
            }
          }

          $data->save();

          return $data;
        } else {
          return $new_data;
        }
    }
}
