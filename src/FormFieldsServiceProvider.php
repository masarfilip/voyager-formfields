<?php

namespace App\Fijeo\FormFields;

use App\Fijeo\FormFields\FormFields\KeyValueJson;
use App\Fijeo\FormFields\FormFields\MultiImages;
use CMS\Voyager\Facades\Voyager;
use Illuminate\Support\ServiceProvider;

class FormFieldsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'fijeo-form-fields');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Voyager::addFormField(MultiImages::class);
        Voyager::addFormField(KeyValueJson::class);

        $this->app->bind(
            'CMS\Voyager\Http\Controllers\VoyagerBaseController',
            'App\Fijeo\FormFields\Controllers\MultiImagesFormFieldsFixController'
        );

        $this->app->bind(
            'CMS\Voyager\Http\Controllers\VoyagerMediaController',
            'App\Fijeo\FormFields\Controllers\MultiImagesFormFieldsFixMediaController'
        );
    }
}
