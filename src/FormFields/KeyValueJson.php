<?php

namespace App\Fijeo\FormFields\FormFields;

use CMS\Voyager\FormFields\AbstractHandler;

class KeyValueJson extends AbstractHandler
{
    protected $codename = 'key-value_to_json';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('fijeo-form-fields::formfields.key_value_json', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }

}
