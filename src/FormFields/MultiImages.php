<?php

namespace App\Fijeo\FormFields\FormFields;

use CMS\Voyager\FormFields\AbstractHandler;

class MultiImages extends AbstractHandler
{
    protected $codename = 'multi_images';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('fijeo-form-fields::formfields.multi_images', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
